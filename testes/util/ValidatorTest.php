<?php

use PHPUnit\Framework\TestCase;

/**
 * Description of JSONTest
 *
 * @author marci
 */
class ValidatorTest extends TestCase
{
        /**
     * @dataProvider dadosURLProvider
     */
    public function testaString($entrada, $esperado)
    {
        $dados = ValidatorUtil::trataArrayDados(['teste'=> $entrada]);
        $this->assertEquals($dados['teste'], $esperado);
    }

    public function dadosURLProvider()
    {
        return [
            'numero' => [
                1,
                1
            ],
            'String simples' => [
                'Texto',
                'Texto'
            ],
            'String com acento' => [
                'Texto é com acento',
                'Texto é com acento'
            ],
            'String com tag' => [
                'Texto <b>é</b> com acento',
                'Texto é com acento'
            ],
            'String com tag quebrada' => [
                'Texto <b>é<b> com acento',
                'Texto é com acento'
            ]
        ];
    }
}

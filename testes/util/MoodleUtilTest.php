<?php

use PHPUnit\Framework\TestCase;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MoodleUtilTest
 *
 * @author marci
 */
class MoodleUtilTest //extends TestCase
{

    /**
     * @dataProvider dadosURLProvider
     */
    public function testaHttpEncode($entrada, $esperado)
    {

       // $this->assertEquals((string) $esperado, '&' . urldecode(http_build_query($entrada)));
    }

    public function dadosURLProvider()
    {
        return [
            'String' => [
                'assignmentids=4992',
                '&assignmentids=4992'
            ],
            'Vetor simples' => [
                ['assignmentids' => [4992]],
                '&assignmentids[0]=4992'
            ],
            'Sem vetor' => [
                ['assignmentids' => 4992],
                '&assignmentids=4992'
            ],
            'Vetor Complexo' => [
                ['assignmentids' => [4992], 'test' => ['teste' => 1]],
                '&assignmentids[0]=4992&test[teste]=1'
            ],
        ];
    }

}

# Tools

As tools são as funcionalidades mais "interessantes" do Enyalius. Essas ferramentas diferencia o Eny dos outros amados frameworks.

Vamos a uma pequena lista:

 * Gerador de CRUD - Isso mesmo a partir de um banco de dados no PostgreSQL você 
tem uma ferramenta que gera o código fonte necessário para ter um CRUD completo. 
 ```
   Acesso: http://server/tool/GeradorDeCrud
 ```
 * DB Installer - Ferramenta que permite criar scripts SQL para levantar o sistema.
Diferente de uma migration essa ferramenta recria o banco deixando o antigo como
 um schema para evitar perda de dados e estrutura.

```
   Acesso: http://server/tool/InstallBD
 ```
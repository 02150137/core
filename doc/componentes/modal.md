## Modal Padrão ##

A modal padrão do Enyalius é ativada da seguinte maneira: 

Modo padrão com páginas externas via AJAX

HTML chamar uma modal
```HTML 
<a href="linkAserCarregado" class="linkModal"> Conteúdo do Link </a>
```

PHP
```php
    
    Componente::load('Modal');
    $modal = new Modal('meuID');
    $this->view->addComponente($modal);
````

#PHP#

O PHP tem alguns métodos que facilitam a criação da modal sem necessidade alterar o modal via js ou template.

```php
    
    $modal = new Modal('meuID');
    $modal->setTitulo('Titulo da modal'); //Altera o título da modal
    $modal->setTipo('AJAX'); //Unico modo implementado atualmente, necessário implementar iframe e conteudo já no DOM.
    $modal->addConf('conf', 'valor'); //Usado para adicionar alguma configuração na modal ver documentação do Bootstrap.
```

#JS#
No JS cada modal recebe um ID (o id padrão é modalGeral).

No exemplo anterior para acessa a modal pode ser usado o comando para mostrar em um evento especifico que não um link.
`enyModal.meuID.show();`

No JS antes de exibir é possível modificar todas as configurações (exeto o ID que é encapsulado) pela propriedad `conf`.

```javascript
enyModal.meuId.conf.titulo = "oi oi vc aí";
```

# Modo full #
No modo full é permitido redimensionar e mover as modais. Para isso é necessário carregar o  JQuery ui por isso por padrão vem desabilitado. 
Para habilitar utilize:

```php
    
    $modal = new Modal('meuID');
    $modal->enableFull();
    
```
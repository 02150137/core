## Windows

Para windows 10 existe o docker desktop que tem recursos bem interessantes para o windows 7 ou 8 usar o docker toolbox:

### Docker Desktop windows 10 (atualizar para 2004 ou mais)

 - Verifique a versão de seu windows se é maior que a 2004, caso for menor baixe o atualizador (ver esse tutorial: https://answers.microsoft.com/pt-br/windows/forum/all/como-atualizar-para-a-vers%C3%A3o-2004-do-windows/241f9840-5ed3-4c45-9731-3bd53ddb7003) que vai bem mais rápido.
 - Baixe e instale o docker 
 - Instale uma distro de preferencia ex: `wsl --install -d ubuntu`
 - Transforme essa distro em principal, ex: `wsl --set-default Ubuntu` (fazendo isso o docker já conversa com a distro) -
 
 Seja feliz!!!


### Docker toolbox

* https://github.com/docker-archive/toolbox

Ao inciar o docker toolbox é necessário instalar o enyalius na pasta dos binários para isso digite as sequencias de comandos.

```
    cd ~
    mkdir bin
    docker run --rm  -v $PWD:/data mwendler/wget --no-check-certificate https://gitlab.com/enyalius/core/raw/master/enyalius -O /data/eny
```



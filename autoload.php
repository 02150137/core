<?php

require_once CORE . 'controller/AutoLoader.class.php';
require_once ROOT . '../vendor/autoload.php';
require_once CORE . 'util/HELPER.php';

use core\controller\AutoLoader;

$loader = new AutoLoader();

$loader->register();

$loader->addNamespace('core', CORE );


$loader->addClass('AbstractController', CORE . 'controller/AbstractController');
$loader->addClass('AbstractView', CORE . 'view/AbstractView');

//Model
$loader->addClass('AbstractModel', CORE . 'model/AbstractModel');
$loader->addClass('Model', CORE . 'model/Model');
$loader->addClass('AbstractDAO', CORE . 'model/AbstractDAO');
$loader->addClass('BDHelper', CORE . 'model/io/BDHelper');
$loader->addClass('Prepared',  CORE . 'model/io/Prepared');

//Util
$loader->addClass('JSON', CORE . 'util/JSON');
$loader->addClass('StringUtil', CORE . 'util/StringUtil');
$loader->addClass('DebugUtil', CORE . 'util/DebugUtil');
$loader->addClass('ValidatorUtil', CORE . 'util/ValidatorUtil');
$loader->addClass('DateUtil', CORE . 'util/DateUtil');
$loader->addClass('MailUtil', CORE . 'util/MailUtil');
$loader->addClass('MoodleUtil', CORE . 'util/MoodleUtil');
$loader->addClass('AvatarUtil', CORE . 'util/AvatarUtil');
$loader->addClass('StorageUtil', CORE . 'util/StorageUtil');

$loader->addClass('Lang', CORE . 'view/Lang');
$loader->addClass('_LOG', CORE . 'libs/log/_LOG');


//add Libs common
$loader->addClass('ArquivoUpload', CORE . 'libs/arquivo/ArquivoUpload');
$loader->addClass('Redimensionador', CORE . 'libs/imagem/Redimensionador');
$loader->addClass('Browser', CORE . 'libs/browser/Browser');
$loader->addClass('PDF', CORE . 'libs/pdf/PDF');
$loader->addClass('Aes', CORE . 'libs/crypto/Aes');


//View
$loader->addClass('Menu', CORE .'view/menu/Menu');
$loader->addClass('MenuItem', CORE .'view/menu/MenuItem');

//interfaces
$loader->addClass('DTOInterface', CORE . 'model/DTOInterface');
$loader->addClass('ObjetoInsercao', CORE . 'model/io/ObjetoInsercao');
$loader->addClass('FileTupleTrait', CORE . 'model/io/FileTupleTrait');
$loader->addClass('KeyValueTrait', CORE . 'model/io/KeyValueTrait');



//Componente
$loader->addClass('Componente', CORE . 'componentes/Componente');

$GLOBALS['loader'] = $loader;

//Chama o AutoLoader de componente
require_once CORE . 'componentes/component_register.php';

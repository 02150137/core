<?php
/**
 * A Interface DTO determina quais métodos precisam ser implementados 
 * em um objeto para se comportar como DTO.
 * 
 * Muitos métodos estão implementados na Trait DTOTrait
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 * @package core.model
 */
interface DTOInterface //extends JsonSerializable
{

    /**
     * Método que retorna o ID do banco daquele objeto em especifico.
     *
     * @return int|misc
     */
    public function getID();

    public function setID($id);
    
    /**
     * Método que retorna o nome da tabela no banco de dados.
     * 
     */
    public function getTable();//Talvez deixe de existir na versão 5
    
    /**
     * Retorna uma string com uma condição para selecionar um item para ser usado 
     * em chaves primárias compostas.
     * 
     * @return string condição para ser executada no banco e identificar um objeto
     */
    public function getCondition(); 
    
    /**
     * Método que retorna um array para o objeto Table.
     * 
     * Isso vai mudar em algum momento em todos os sistemas.
     * 
     * @return array dados que serão exibidos na tabela 
     */
    public function getArrayJson();
    
    public function getUpdateArray();

    public function getDataArray();
    
}

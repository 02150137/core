<?php

/**
 * Description of AbstractModel
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 * @package core.model
 */
abstract class AbstractModel
{

    /**
     *
     * @var BDHelper
     */
    protected $db;
    protected $erro = 'Erro não identificado';

    /**
     * 
     * @param String $dbName nome do banco de dados
     * @param type $helper
     * @param type $server
     * @param type $user
     * @param type $pass
     */
    public function __construct(
        $dbName = DB_NAME,
        $server = DB_SERVER,
        $user = DB_USER,
        $pass = DB_PASSWORD
    ) {
        $this->registerRequires();
        if (DB_TYPE == 'pgsql') {
            $this->db = new PGHelper($dbName, $server, $user, $pass);
        } else {
            $this->db = new BDHelper($dbName, DB_TYPE, $server, $user, $pass);
        }
    }

    public function pdo(){
        return $this->db->pdo();
    }

    /**
     * Método que retorna os erros ao tentar realizar operaçoes no banco. 
     *
     * @return []
     */
    public function getErros()
    {
        return $this->db->getLogErrors();
    }

    /**
     * Método que processa o erro do 
     *
     * @return void
     */
    public function getErro()
    {
        #TODO processar os erros do banco para não exibir informações sensiveis 
        return $this->erro;
    }

    public function getSingleRow($sql)
    {
        $dados = $this->query($sql . ' LIMIT 1' );
        return $dados->fetch();
    }

    /**
     * Retorna uma row de uma tabela 
     * 
     * ATENÇÃO: retorna sempre uma linha apenas devido ao LIMIT.
     *
     * @param string $table
     * @param string $condition
     * @return []
     */
    public function getTableRow($table, $condition)
    {
        return $this->queryTable($table, '*', $condition, null, 1)->fetch();
    }

    /**
     * Retora a consulta PDO do banco conectado
     * 
     * @param string $sql
     * @return PDOStatement
     */
    public function query($sql)
    {
        return $this->db->query($sql);
    }

    /**
     * Método que retorna o ponteiro para o banco de dados 
     * 
     * @return BDHelper
     */
    public function DB()
    {
        return $this->db;
    }

    /**
     * Método que executa o query table do objeto de banco de dados
     * 
     * Atenção uma string pode conter sql injection uma alternativa é usar Prepared statement
     * para isso utilize Prepared::condition('condicao = ?', [...])
     * 
     * @param string $table - nome da tabela de busca
     * @param misc $fields
     * @param string|array $condition
     * @param misc $order
     * @param int $limit
     * @param int $offset
     * @return PDOStatement
     */
    public function queryTable($table, $fields = '*', $condition = null, $order = null, $limit = null, $offset = null)
    {
        return $this->db->queryTable($table, $fields, $condition, $order, $limit, $offset);
    }

    /**
     * Método retrocompativel com resultadoAssoc depreciado usar
     * 
     * foreach($consulta as $linha)
     * 
     * @deprecated since version 1.0
     * @param type $result
     * @return type
     */
    public function resultAssoc(PDOStatement $result)
    {
        return $result->fetch();
    }


    public function rowCount(PDOStatement $consulta)
    {
        return count($consulta->fetchAll());
    }

    /**
     * Cria um mapa de dados simples para se utilizar em campos de
     * seleção e etc. O modelo do mapa é $mapa['INDICE_DO_DADO'] = 'VALOR_DO_DADO'
     *
     * @param Resource $consulta
     * @param string $indice
     * @param String $valor
     * @return array Mapa com os dados
     */
    public function getMapaSimplesDados($consulta, $indice, $valor)
    {
        $array = array();
        if ($consulta) {
            foreach ($consulta as $linha) {
                $array[$linha[$indice]] = $linha[$valor];
            }
        }
        return $array;
    }


    private function registerRequires()
    {
        /**
         * @var AutoLoader
         */
        $loader = $GLOBALS['loader'];
        $loader->addClass('PGHelper', CORE . 'model/io/PGHelper');
        $loader->addClass('BDHelper', CORE . 'model/io/BDHelper');
    }
}

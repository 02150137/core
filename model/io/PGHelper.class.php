<?php

/**
 * Classe com as expecificações do PostgreSQL para o PDO.
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 * @package core.model.io
 */
class PGHelper extends BDHelper
{

    public function __construct($dbName, $server, $user, $pass)
    {
        parent::__construct($dbName . ";options='-c client_encoding=utf8'", 'pgsql', $server, $user, $pass);
    }

    /**
     * Retorna o próximo valor da tabela e registra esse numero para 
     * a operação garantindo a consistência dos dados e a normalização dos mesmos.
     *
     * @param String  $tabela = Nome da tabela que fornecerá o próximo valor
     * @return Integer $proximoValor = Valor do próximo valor auto-incrementável.
     */
    public function nextValue($tabela)
    {
        $tabelaComSchema = explode('.', $tabela);
        if (sizeof($tabelaComSchema) == 2) {
            $query = "SELECT nextval('" . $tabela . '_id_' . $tabelaComSchema[1] . "_seq'::regclass)";
        } else {
            $query = "SELECT nextval('" . $tabela . '_id_' . $tabela . "_seq'::regclass)";
        }
        $result = $this->query($query);
        if ($result) {
            $array = $result->fetch();
            return $array['nextval'];
        }
        return false;
    }

    public function saveFile($table, $colunm, $file, $extras = array())
    {
        if (isset($extras['type']) && $extras['type'] == 'bytea') {
            //FIXME gerar código para  inserir como bytea
        } else { //oid
        }
    }

    /**
     * Método que prepara o LargeObject ou o array de bytes para salvar em coluna
     * 
     * @param type $file
     * @param type [$type] Tipo do salvamento em coluna byte ou largeObject - Padrão lo
     */
    public function saveFileInColunm($file, $type = 'lo')
    {
        if ($type == 'bytea') {
            //FIXME gerar código para  inserir como bytea
        } else { //oid
            return $this->writeOID($file);
        }
    }

    /**
     * 
     * @param string $file
     * @return type
     * @throws SQLException
     */
    private function writeOID($file)
    {
        $microTrans = 0;
        if (!$this->inTransaction()) {
            $microTrans = 1;
            $this->database->beginTransaction();
        }
        $oid = $this->database->pgsqlLOBCreate();
        $stream = $this->database->pgsqlLOBOpen($oid, 'w');
        $local = fopen($file, 'rb');
        stream_copy_to_stream($local, $stream);
        $local = $stream = null; //Limpa os ponteiros
        if ($microTrans) {
            $this->database->commit();
        }
        return $oid;
    }

    private function readOID($oid)
    {
        return $this->database->pgsqlLOBOpen($oid, 'r');
    }

    /**
     * Método que le um arquivo do banco de dados
     * 
     * @param string $table - nome da tabela
     * @param string $colunm
     * @param int $id - seletor
     * @param type $extras - Opções extras que podem ser usados 
     * @throws ProgramacaoException
     */
    public function readFile($table, $colunm, $id, $extras = array())
    {
        $tableF = explode('.', $table);
        $str = sizeof($tableF) > 1 ? $tableF[1] : $tableF[0];
        $query = $this->queryTable($table, $colunm, 'id_' . $str . ' = ' . $id);
        if ($linha = $query->fetch()) {
            #FIXME verificar se é algo diferente de OID
            return $this->readOID($linha[$colunm]);
        }
    }
}

<?php

class Prepared{

   
    private $query = '';
    private $data = [];

    public function __construct($query, $data)
    {
        $this->query = $query;
        $this->data = $data;
    }
    

    public static function condition($query, $data){
        if(preg_match('/[?:]/', $query) && !empty($data)){
            return new Prepared($query, $data);
        }else{
            dd('Deu problema na query preparada');
        }
    }

    /**
     * @return array
     */ 
    public function getData()
    {
        return $this->data;
    }

    public function __toString()
    {
        return $this->query;
    }
}
<?php

namespace core\model;

use StringUtil;

/**
 * A DTO Trait é uma trait que tem por função dar manipular de forma mais adequada
 * e de forma mais transparente todos atributos de um objeto DTO. 
 * 
 * Essa trait implementa muitos comportamentos do DTOInterface. Deixando de forma 
 * transparente a utilização dos objetos de manipulação do banco.
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 3.0
 * @package core.model
 */
trait DTOTrait
{
    private $ignoreFields = ['id', 'id' . __CLASS__];
    private $table;
    /**
     * Vetor que armazena os erros ao tentar setar dados via métodos 
     * de builder.
     *
     * @var array
     */
    private $_ERROS_BUILDER = [];
    private $_notSetAlowed = [];

    /**
     * Utiliza como condição de seleção a chave primária
     *
     * @return String - Condição para selecionar um dado unico na tabela
     */
    public function setID($id)
    {
        if (!property_exists($this, 'id')) {
            $metodo = 'setId' . self::DTOClass();
            $this->{$metodo}($id);
        } else {
            $this->id = $id;
        }
        return $this;
    }

    /**
     * Retorna o valor do ID do objeto 
     *
     * @return misc - valor da 
     */
    public function getID()
    {
        if (!property_exists($this, 'id')) {
            $metodo = 'getId' . self::DTOClass();
            return $this->{$metodo}();
        }
        return $this->id;
    }

    public static function getAtributes()
    {
        $colunas = [];
        $classe = __CLASS__;
        $obj = new $classe();
        $ignore = array_merge($obj->ignoreFields, ['table', 'ignoreFields', 'isValid']);
        foreach (get_object_vars($obj) as $key => $value) {
            if (!in_array($key, $ignore) && $key[0] != '_' ) {
                $colunas[] = $key;
            }
        }
        return $colunas;
    }

    /**
     * Retorna o valor da variável $tabela
     *
     * @return string - Tabela do SGBD
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * Retorna o nome da tabela no SGBD de forma estática
     *
     * @return string - Tabela do SGBD
     */
    public static function table()
    {
        $classe = __CLASS__;
        $obj = new $classe();
        return $obj->getTable();
    }

    /**
     * Adiciona em tempo de execução um campo para ser ignorado. 
     * 
     * @param String $field nome do campo a ser ignorado
     */
    public function ignoreField($fields)
    {
        $args = func_get_args();
        if (is_array($fields)) {
            $this->ignoreFields = array_merge($this->ignoreFields, $fields);
        } else if (sizeof($args) > 1) {
            foreach ($args as $field) {
                $this->ignoreFields[] = $field;
            }
        } else {
            $this->ignoreFields[] = $fields;
        }
    }

    /**
     * Campos que deverão ser removidos do vetor de campos ignorados
     * 
     * @param misc $values
     */
    public function removeIgnored($values)
    {
        $args = func_get_args();
        $fields = [];
        foreach ($args as $field) {
            if (is_array($field)) {
                $fields = array_merge($fields, $field);
            } else {
                $fields[] = $field;
            }
        }
        $this->ignoreFields = array_diff($this->ignoreFields, $fields);
    }

    /**
     * Campos que deverão ser removidos do vetor de campos ignorados
     * 
     * @param misc $values
     * @deprecated 2.0 - use removeIgnored
     */
    public function considereField($values)
    {
        $this->removeIgnored($values);
    }

    /**
     *  Método que realiza um lazyLoad de um ou vários objetos
     * 
     * @param misc $classe - Classe ou objeto 
     * @param misc $condicao - Restricao do lazyLoad
     */
    public function lazyLoad($classe, $condicao, $order = false, $limit = false, $key = false)
    {
        $var = '_' . $classe;
        if (!isset($this->{$var})) {
            $this->{$var} = null;
        }

        if (is_null($this->{$var})) {
            $daoName = $classe . 'DAO';
            $dao = new $daoName();
            $cond = is_numeric($condicao) ? $dao->getColunmID() . ' = ' . $condicao : $condicao;
            $lista = $dao->getList($cond, $order, $limit, $key);
            if (sizeof($lista) == 1) {
                $this->{$var} = $lista[0];
            } else {
                $this->{$var} = $lista;
            }
        }
        return $this->{$var};
    }

    /**
     * Método mágico get que permite acesso direto a uma propriedade do objeto 
     * 
     * OBS: São super protegidas propriedades que começarem pelo underscore $_propriedade
     * 
     * OBS2: Caso tenha um método getPropriedade() a mesma será usada para acesso a propriedade
     *
     * @param [type] $name
     * @return void
     */
    public function __get($name)
    {
        //Se existir getObjeto
        $method = 'get' . ucfirst($name);
        if (method_exists($this, $method)) {
            return $this->{$method}();
        }

        if (isset($this->{$name}) && $name[0] != '_') {
            return $this->{$name};
        }
        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $name .
                ' in ' . $trace[0]['file'] .
                ' on line ' . $trace[0]['line'],
            E_USER_WARNING
        );
        return null;
    }

    /**
     * Set mágico que permite acesso direto a uma propriedade do objeto 
     * 
     * OBS: São super protegidas propriedades que começarem pelo underscore $_propriedade
     * 
     * OBS2: Caso tenha um método setPropriedade() a mesma será usada para acesso a propriedade
     *
     * @param [type] $name
     * @param [type] $value
     */
    public function __set($name, $value)
    {
        $realPrivate = $this->_notSetAlowed;
        if (!in_array($name, $realPrivate) &&  $name[0] != '_') {
            $metodoName = 'set' . ucfirst($name);
            if (method_exists($this, $metodoName)) {
                return $this->{$metodoName}($value);
            }
            $this->{$name} = $value;
            return $this;
        }
        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $name .
                ' in ' . $trace[0]['file'] .
                ' on line ' . $trace[0]['line'],
            E_USER_WARNING
        );
    }

    /**
     * Método que permite usar get e set mágico mesmo 
     * usando sintaxe de get e set do Javinha
     *
     * @param [type] $metodo
     * @param [type] $args
     * @return void
     */
    public function __call($metodo, $args)
    {
        if (strpos($metodo, 'set') === 0) {
            $prop = lcfirst(str_replace('set', '', $metodo));
            if (property_exists($this, $prop)  && $prop[0] != '_') {
                $this->{$prop} = $args[0];
                return $this;
            }
        }
        if (strpos($metodo, 'get') === 0) {
            $prop = lcfirst(str_replace('get', '', $metodo));
            if (property_exists($this, $prop) && $prop[0] != '_') {
                return $this->{$prop};
            }
        }

        $trace = debug_backtrace();
        trigger_error(
            'Undefined method via __call(): ' . $metodo .
                ' in ' . $trace[0]['file'] .
                ' on line ' . $trace[0]['line'],
            E_USER_WARNING
        );
    }

    /**
     * Alias para manter a retrocompantibilidade
     *
     * @return void
     */
    public function getArrayJSON()
    {
        return $this->jsonSerialize();
    }

    /**
     * Método padrão para serializar um objeto em JSON 
     * basta implementa a interface na classe
     *
     * @return []
     */
    public function jsonSerialize(): mixed
    {
        $data = [];
        foreach ($this as $prop => $value) {
            if ($this->verificaCampo($prop, $value)) {
                $data[$prop] = $value;
            }
        }
        return $data;
    }

    /**
     * Retorna um array com o formato 
     *     "campo_tabela" => "valor" 
     * 
     * para inserir no banco
     * 
     * @return array - Array de dados para inserir 
     */
    public function getDataArray($ignoreID = true)
    {
        $campos = [];
        foreach ($this as $chave => $valor) {
            if ($this->verificaCampo($chave, $valor, $ignoreID)) {
                $mapa = self::DAO()->getColunms();
                $indice =  isset($mapa[$chave]) ?  $mapa[$chave] : \StringUtil::underscoreNumber($chave);
                //Se for false o banco não faz conversão automatica de vazio para false por isso é necessário enviar um 0
                $campos[$indice] = $valor === false ? '0' : $valor;
            }
        }
        return $campos;
    }

    /**  
     * Método que recebe o array de dados do banco e popula. 
     * O objeto sera preenchido com apenas os dados do array.
     * 
     * O padrão do banco atributo_composto é mapeado para atributoComposto
     * Caso tiver dados a mais o objeto armazenará 
     *
     * @param array $array
     * @return self
     */
    public static function builderData($array)
    {
        $classe = __CLASS__;
        $obj = new $classe();
        $obj->setDataArray($array);
        return $obj;
    }

    /**
     * Método que recebe o array de dados do banco e popula. 
     * O objeto sera preenchido com apenas os dados do array.
     * 
     * O padrão do banco atributo_composto é mapeado para atributoComposto
     * Caso tiver dados a mais o objeto armazenará um novo campo
     *
     * @param array $array
     */
    public function setDataArray($array)
    {
        foreach ($array as $attr => $value) {
            if (is_int($attr)) {
                continue;
            }
            //ver de usar os get e sets
            $this->{StringUtil::toCamelCase($attr)} = $value;
        }
    }

    /**
     * Alias para getDataArray
     * 
     * @deprecated 1.2.0 use getDataArray
     * @return array
     */
    public function getArrayDados()
    {
        return $this->getDataArray();
    }

    /**
     * Retorna um array com o formato 
     *     "campo_tabela" => "valor" 
     * 
     * para atualizar tupla em uma tabela no banco
     * 
     * @return array - Array de dados para atualizar 
     */
    public function getUpdateArray()
    {
        $campos = [];
        foreach ($this as $chave => $valor) {
            if ($this->verificaCampo($chave, $valor) && $valor !== null) {
                $campos[\StringUtil::underscoreNumber($chave)] = $valor === false ? '0' : $valor;
            }
        }
        return $campos;
    }

    /**
     * Undocumented function
     *
     * @deprecated version 1.2.0 use getUpdateArray
     * @return void
     */
    public function getArrayAtualizar()
    {
        return $this->getUpdateArray();
    }

    /**
     * Popula o objeto recebendo um array no formato
     * "nomeCampo" => "valor" 
     * 
     * #TODO criar vetor de erros.
     *
     * @param array $array - Array de 
     * @return int - número de erros encontrados
     */
    public function setArrayDados(array $array)
    {
        $erros = 0;
        foreach ($array as $campo => $valor) {
            $metodo = 'set' . ucfirst($campo);
            if (property_exists($this, $campo) && !$this->{$metodo}($this->trataValor($valor))) {
                $erros++;
                $this->isValid = false;
            }
        }
        return $erros;
    }

    /**
     * Retorna apenas um objeto da classe Repassada
     * 
     * @param int $id
     * @return Object
     */
    public static function getOne($id)
    {
        return self::DAO()->getById($id);
    }

    /**
     * Retorna apenas um objeto da classe Repassada
     * 
     * @param string $condition
     * @return Object
     */
    public static function getOneByCondition($condition)
    {
        return self::DAO()->getOneByCondition($condition);
    }

    /**
     * Método que atualiza ou salva o objeto 
     * 
     * @param bool $transaction - false - Permite 
     */
    public function save($transaction = false)
    {
        $dao = self::DAO();
        if ($transaction) {
            return $dao->saveWithId($this);
        }
        return $dao->save($this);
    }

    public function create()
    {
        return self::DAO()->create($this);
    }

    /**
     * Carrega dados da linha da tabela para um objeto em especifico.
     * 
     * @param int $id
     */
    public function load($id)
    {
        $obj = self::getOne($this->getID());
        foreach ($this as $chave => $valor) {
            $this->{$chave} = $obj->{$chave};
        }
    }

    public function delete()
    {
        return self::DAO()->delete($this);
    }

    /**
     * Retorna uma lista de todos os objetos da tabela
     * 
     * A condição deve ser usada para restrições do sistema (ex.  apenas objetos de um id_user específico)
     * para listas complexas usar o getList
     * 
     * @param misc $condition
     * @return []
     */
    public static function getAll($condition = false, $order = false)
    {
        return self::DAO()->getList($condition, $order);
    }

    /**
     * Forma de acesso estático ao método getList da classe DAO.
     * Método mais completo que permite ordenação e limit.
     *
     *
     * @param misc $condition
     * @param string [$order] Se informado aplica a ordenação do argumento
     * @param string [$limit] Se informado aplica o limite do argumento
     * @param string [$key] Se informado utiliza o campo como chave do array de retorno
     * @return []
     */
    public static function getList($condition = false, $order = false, $limit = false, $key = false)
    {
        return self::DAO()->getList($condition, $order, $limit, $key);
    }

    /**
     * Retorna um mapa (id =>valor) de todos os objetos que atendem uma condição
     * 
     * @param type $condition
     * @return type
     */
    public static function getMap($condition = false, $order = false)
    {
        return self::DAO()->getListWithID($condition, $order);
    }

    /**
     * Recebe valores através de variável valor e caso necessário
     * faz o parser dos objetos como o caso de objetos geográficos ou arquivos.
     * 
     * @param misc $valor
     * @return misc pode ser o próprio valor ou um objeto.
     */
    private function trataValor($valor)
    {
        if (is_string($valor) && strpos($valor, 'POINT(') !== false) {
            return new \Point($valor);
        }
        return $valor;
    }

    /**
     * Verifica se o parametro do objeto vai ser usado para o 
     * mapeamento objeto relacional. 
     *       
     * @param string $chave
     * @param misc $valor
     * @return boolean
     */
    private function verificaCampo($chave, $valor)
    {
        //TODO Fazer teste de performance memória x processamento (Aqui se deu preferencia por processamento). 
        //Ver de jogar para cima já que vai fazer o teste 
        return (($chave != 'isValid') &&
            ($chave != 'table') &&
            ($chave != 'ignoreFields') &&
            ($chave[0] != '_') &&
            !is_array($valor) &&
            !in_array($chave, $this->ignoreFields)
        );
    }

    /**
     * Retorna o nome da classe DTO para usar o respectivo DAO.
     * 
     * @return String
     */
    private static function DTOClass()
    {
        $class = __CLASS__;
        if (strrpos($class, '\\') !== false) {
            $class = substr(__CLASS__, strrpos(__CLASS__, '\\') + 1);
        }
        return $class;
    }

    /**
     * Retorna o nome da classe DTO para usar o respectivo DAO.
     * 
     * @return AbstractDao
     */
    public static function DAO()
    {
        $classe = self::DTOClass() . 'DAO';
        return new $classe();
    }
}

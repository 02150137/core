<?php

namespace core\model;

/**
 *  Trait que implementa a funcionalidade de adicionar/gerenciar informações na
 *  em um vetor de forma transparente ao usuário 
 * 
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 */
trait KeyValueTrait
{

    private $_DATA = [];
    /**
     * Adiciona um dado na sessão 
     * 
     * @param String $key
     * @param misc $value
     */
    public function add($key, $value = null)
    {
        if(is_null($value)){
            return  $this->_DATA[$key] = $key;
        }
        $this->_DATA[$key] = $value;
    }

    /**
     * Método que retorna uma variável salva na sessão.
     * 
     * Retorna nulo em caso de não existencia
     * 
     * @param String $key
     * @return misc
     */
    public function get($key)
    {
        if (isset($this->_DATA[$key])) {
            return $this->_DATA[$key];
        }
        return null;
    }

    public function verify($key)
    {
        return isset($this->_DATA[$key]);
    }

    /**
     * Deleta o campo da sessão
     * 
     * @param String $key
     */
    public function del($key)
    {
        unset($this->_DATA[$key]);
    }
    
    public function addDataArray($array){
        if(is_array($array)){
            $this->_DATA = array_merge($this->_DATA, $array);
        }
    }

}

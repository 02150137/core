<?php

/**
 * A classe AbstractDAO possui os métodos bases dos objetos DAO.
 *
 * Também determina que métodos devem ser gerados de forma especificas em cada um
 * dos objetos filhos.
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 2.2.1
 * @package core.model
 */
abstract class AbstractDAO extends AbstractModel
{
    protected $table, $colunmID, $colunms;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método que retorna um ponteiro para um arquivo ou null caso alguma condição
     * não for satisfeita.
     *
     * @param $colunaArquivo
     * @param $id
     */
    public function getFile($colunaArquivo, $id)
    {
        $this->DB()->begin();
        return $this->DB()->readFile($this->getTable(), $colunaArquivo, $id);
    }

    /**
     * Método que insere se for um objeto novo ou atualiza se for um existente
     *
     * @param DTOInterface $object
     * @return bool
     */
    public function save(DTOInterface $object)
    {
        $id = $object->getID();
        if (empty($id)) {
            return $this->create($object);
        } else {
            return $this->update($object);
        }
    }

    /**
     * Método que insere um objeto em sua respectiva tabela no banco de dados
     *
     * Por padrão o ID da tabela é ignorado para usar o valor default de auto incremento
     * caso queira usar a propriedade ID utilize o método $dto->removeIgnored('idDTO')
     * ou $dto->removeIgnored('id') dependendo de seu modelo.
     *
     * @param DTOInterface Objeto data transfer
     */
    public function create(DTOInterface $object)
    {
        return $this->db->insert($object->getTable(), $object->getDataArray());
    }

    /**
     * Método que atualiza um objeto no banco de dados.
     *
     * Em caso de chaves compostas deve-se atentar se o método DTO::getCondition
     * foi corretamente sobreescrito.
     *
     * @param DTOInterface $object
     * @return type
     */
    public function update(DTOInterface $object)
    {
        return $this->db->update($object->getTable(), $object->getUpdateArray(), $object->getCondition());
    }

    /**
     * Retorna um array com todos os objetos.
     *
     * @return Array DTOInterface
     */
    public function getAll($condicao = false)
    {
        return $this->getList($condicao);
    }

    /**
     * Retorna um array com todos os objetos de acordo com a condição.
     *
     * @deprecated 2.0 use getList
     * @return Array DTOInterface
     */
    public function getAllByCondition($condicao)
    {
        return $this->getList($condicao);
    }

    /**
     * Alias para getById pode ser sobscrito para se utilizar o comparable.
     *
     * @param DTOInterface $object
     * @return type
     */
    public function getOne(DTOInterface $object)
    {
        return $this->getByID($object->getID());
    }


    /**
     *
     * @param string $condicao
     * @return DTOInterface
     */
    public function getOneByCondition($condicao)
    {
        try {
            $lista = $this->getList($condicao);
            if ($lista) {
                #TODO issue #17 - Verificar a performance de logar se caso retornou mais de um resultado.
                return $lista[0];
            }
            return false;
        } catch (ErrorException $e) {
            ds('LastQuery: ' . $this->DB()->getLastQuery());
            echo $e->getTraceAsString();
        }
    }

    /**
     *
     * Depreciado pois foi implementado no gerador de CRUD o método especifico que
     * retorna um array de objetos com a chava como ID.
     *
     * @param type $coluna
     * @param DTOInterface $object
     * @return type
     * @deprecated since version 2.0
     */
    public function getMapa($coluna, DTOInterface $object)
    {
        return $this->getMapaSimplesDados($this->queryTable($object->getTable()), $object->getID(), $coluna);
    }

    /**
     * Remove o objeto do banco de dados
     *
     * @param DTOInterface $object
     * @return boolean
     */
    public function delete(DTOInterface $object)
    {
        return $this->db->delete($object->getTable(), $object->getCondition());
    }

    /**
     * Retorna um objeto completo do DAO corrente
     * 
     * @param misc $id
     */
    public function getByID($id)
    {
        $consulta = $this->queryTable($this->getTable(), $this->getColunmsString(), $this->getColunmID() . '=' . $id);
        $data = $consulta->fetch();
        if ($data) {
            $objeto = $this->setDados($data);
            return $objeto;
        } else {
            throw new EntradaDeDadosException('Id inválido ' . $id);
        }
    }

    /**
     * Método que insere um objeto do tipo DTOInterface
     * na tabela do banco de dados
     *
     * @param DTOInterface Objeto data transfer
     * @deprecated 2.2.1 use saveWithId
     */
    public function inserirEmTransacao(DTOInterface $obj)
    {
        return $this->saveWithId($obj);
    }

    /**
     * Método que insere um objeto do tipo DTOInterface
     * na tabela do banco de dados
     *
     * @param DTOInterface Objeto data transfer
     */
    public function saveWithId(DTOInterface $obj)
    {
        $this->DB()->begin();
        try {
            if ($this->save($obj)) {
                $sequencia = $this->getTable() . '_' . $this->getColunmID() . '_seq';
                $id = $this->DB()->lastInsertId($sequencia);
                $this->DB()->commit();
                $obj->setID($id);
                return $id;
            }
        } catch (Exception $e) {
            //ds($e);
            _LOG::error('Transação inválida: ', $this->DB()->getLogErrors());
        }
        $this->DB()->rollback();
        return false;
    }

    /**
     *
     * @deprecated since version 3.0 Usar getList mantido para retrocompatibilidade.
     * @param misc $condicao
     * @return array
     */
    public function getLista($condicao)
    {
        return $this->getList($condicao);
    }

    /**
     * Método que permite retornar listas de DAOs especificos
     *
     * Método mais completo que permite ordenação e limit.
     *
     *
     * @param misc $condition
     * @param string [$order] Se informado aplica a ordenação do argumento
     * @param string [$limit] Se informado aplica o limite do argumento
     * @return array
     */
    public function getList($condition, $order = false, $limit = false, $key = false)
    {
        $query = $this->queryTable($this->getTable(), $this->getColunmsString(), $condition, $order, $limit);
        return $this->listByQuery($query, $key);
    }

    /**
     * Retorna um array com a tabela dos dados para o componente de tabela
     *
     * @deprecated 2.2 - Usar getQueryTable permite join e restricoes
     * @return array tabela de dados
     */
    public function getTabela(TabelaConsulta $tabela)
    {
        //Conta o número de linhas
        $nLinhas = $this->countRowsTable($this->getTable());
        //Monta query
        $result = $this->queryTable(
            $this->getTable() . ' ' . $tabela->getcondicao(),
            $this->getColunmsString()
        );

        return $this->getJsonJQGRID($tabela, $result, $nLinhas);
    }

    /**
     * Tabela opcional de join e restrição opcional
     *
     * @param TabelaConsulta $queryTable
     * @param boolean $aditionalRestriction
     * @param boolean $aditionalTable
     * @return array - Array para JSON
     */
    public function getQueryTable(TabelaConsulta $queryTable, $aditionalRestriction = false, $aditionalTable = false)
    {
        $table = $this->getTable();
        //Monta nome da tabela caso de join
        if ($aditionalTable) {
            $table .= ' NATURAL JOIN ' . $aditionalTable;
        }
        //Conta o número de linhas
        $nRows = $this->countRowsTable($table, $aditionalRestriction);
        if ($aditionalRestriction) {
            $queryTable->restricaoExtra($aditionalRestriction);
        }
        //Monta query
        $result = $this->queryTable(
            $table . ' ' . $queryTable->getcondicao(),
            $this->getColunmsString()
        );

        return $this->getJsonJQGRID($queryTable, $result, $nRows);
    }

    private function getJsonJQGRID(TabelaConsulta $tabela, $result, $countRows)
    {
        $dados = [];
        $resultado = [
            'page' => $tabela->getPagina(),
            'total' => $tabela->calculaPaginacao($countRows),
            'records' => $countRows,
        ];
        foreach ($result as $linhaBanco) {
            $row = [];
            $objeto = $this->setDados($linhaBanco);
            $id = $objeto->getID();
            $row['id'] = $id;
            $row['cell'] = $objeto->getArrayJson();
            array_unshift($row['cell'], $id);
            $dados[] = $row;
        }
        $resultado['rows'] = $dados;
        return $resultado;
    }

    /**
     * Conta quantas linhas inseridas uma tabela possui.
     *
     * @param string $table
     * @param string $condition
     * @return int
     */
    public function countRowsTable($table, $condition = false)
    {
        $nLinhasCon = $this->queryTable($table, 'count(*)', $condition);
        return $nLinhasCon->fetch()[0];
    }

    /**
     * Método proxy
     * 
     * Depreciado na criação tem que repensar essa estrutura.
     * 
     * @deprecated Issue #
     * @param array $array
     * @return misc 
     */
    public function setDadosProxy($array){
        return $this->setDados($array);
    }

    /**
     * Método que precisa ser sobscrito para cada um dos DAOS
     * 
     * @param array $array
     */
    abstract protected function setDados($array); //Somente quando tiver todo o sistema testado

    /**
     * Método que retorna uma string montando o nome das colunas que devem ser buscadas
     * na tabela especifica
     *
     * @return array
     */
    public function getColunms()
    {
        return $this->colunms;
    }

    /**
     * Método util para gerar a consulta que deve ser realizada no banco de dados
     * facilita a manipulação interna sem necessidade de mudar vários trechos no código
     * 
     * @return string
     */
    public function getColunmsString()
    {
        $string = $this->getColunmID() . ' as principal, ';
        $string .= rtrim(implode(', ', $this->getColunms()), ',');
        return $string;
    }

    public function getColunmTable($property)
    {
        if(isset($this->colunms[$property])){
            return $this->colunms[$property];
        }
        return StringUtil::toUnderscore($property);
    }

    /**
     * Retorna uma lista de objetos podendo ser indexados pela chave primária ou não
     * 
     * Para retornar com a chave primário o argumento opcional deve estar setado como true.
     * 
     * @param PDOStatement $query
     * @param bool $key
     * @return type
     */
    public function listByQuery($query, $key = false)
    {
        $dados = array();
        if ($query) {
            foreach ($query as $linhaBanco) {
                $objeto = $this->setDados($linhaBanco);
                if ($key) {
                    $dados[$linhaBanco[$key]] = $objeto;
                } else {
                    $dados[] = $objeto;
                }
            }
        }
        return $dados;
    }

    public function listByRawQuery($query, $key = false)
    {
        return $this->listByQuery($this->query($query), $key);
    }

    /**
     * Método que permite retornar um array passando o nome da view.
     *
     * Importante a view deve possuir as mesmas colunas do objeto e ter uma
     * coluna denominada principal que deve ser uma chave única.
     *
     * @param [String] $view
     * @param [misc] $condition
     * @param [misc] $order
     * @param [misc] $limit
     * @param [misc] $key
     * @return array
     */
    public function listByTableView($view, $condition = false, $order = false, $limit = false, $key = false)
    {
        $query = $this->queryTable($view, $this->getColunmsString(), $condition, $order, $limit);
        return $this->listByQuery($query, $key);
    }

    /**
     * Retorna todos os resultados mas com o ID como chave da lista
     *
     * @param misc $condition Description
     * @deprecated since version 2.1
     */
    public function getAllWithID($condition = false)
    {
        return $this->getListWithID($condition);
    }

    /**
     * Retorna todos os resultados mas com o ID como chave da lista
     *
     * @param misc $condition Description
     */
    public function getListWithID($condition = false, $order = false)
    {
        $dados = array();
        $result = $this->queryTable($this->getTable(), $this->getColunmsString(), $condition, $order);
        if ($result) {
            foreach ($result as $linhaBanco) {
                $objeto = $this->setDados($linhaBanco);
                $dados[$linhaBanco['principal']] = $objeto;
            }
        }
        return $dados;
    }

    /**
     * Seta qual a tabela deverá ser mapeada pelo objeto DAO.
     *
     * @param String $table
     */
    public function setTable($table)
    {
        $this->table = $table;
        return $this;
    }

    /**
     * Nome da tabela
     *
     * @return String
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     *
     * @return nome da coluna ou colunas que reprensentam o ID da tabela
     */
    public function getColunmID()
    {
        return $this->colunmID;
    }
}

<?php

/**
 * Model of Controller of system
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.1
 * @package core.controller 
 */
#[AllowDynamicProperties]
abstract class AbstractController
{

    use \core\controller\StorageTrait;
    use \core\controller\ToolsTrait;

    /**
     *
     * @var AbstractView  
     */
    protected $view;

    /**
     * Método padrão chamado para o controlador caso a ação não seja especificada
     */
    public abstract function index();

    /**
     * Método que executa caso encontra um controlador mas não a sua ação
     * 
     * @return void
     */
    public abstract function paginaNaoEncontrada();

    /**
     * 
     * @param string $url
     */
    public function redirect($url)
    {
        if (!empty($url)) {
            header('Location: ' . $url);
        }
    }

    /**
     * Retorna o objeto de view para proteger a troca.
     *
     * @return AbstractView
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * Captura todos os argumentos extras passados para o método via URL
     * 
     * @param int $pos posição do argumento extra começa com 0
     * @param CONST $filter Filtro de Sanitize que pode ser utilizado por padrão Trata uma string
     * @return string
     */
    protected function getArg($pos, $tipo = FILTER_UNSAFE_RAW)
    {
        if (isset($GLOBALS['ARGS'][$pos])) {
            return filter_var($GLOBALS['ARGS'][$pos], $tipo);
        }
        return '';
    }

    /**
     * Array com os argumentos passados na URL.
     *
     * @return array
     */
    protected function getArgs()
    {
        if (isset($GLOBALS['ARGS'])) {
            return $GLOBALS['ARGS'];
        }
        return [];
    }

    /**
     * Método que permite adicionar um argumento para demais métodos. 
     * Útil em redirects
     *
     * @param misc $value
     * @param [int] $pos
     * @return void
     */
    protected function putArg($value, $pos = false)
    {
        if ($pos !== false) {
            $GLOBALS['ARGS'][$pos] = $value;
        } else {
            $GLOBALS['ARGS'][] = $value;
        }
    }

    /**
     * Método que remove algum arg para passar de um método para outro.
     *
     * @param [type] $pos
     * @return void
     */
    protected function unsetArg($pos)
    {
        unset($GLOBALS['ARGS'][$pos]);
    }

    /**
     * Atalho para facilitar acesso a IDs 
     *
     * @param int $pos
     * @return int
     */
    protected function getIntArg($pos)
    {
        $val =  $this->getArg($pos, FILTER_SANITIZE_NUMBER_INT);
        if ($val === '') {
            return false;
        }
        return $val;
    }

    /**
     * Garante o retorno de um Int.
     * 
     * @param misc $value
     * @return int
     */
    protected function trataInt($value)
    {
        return filter_var($value, FILTER_SANITIZE_NUMBER_INT);
    }

    /**
     * Trata o campo para não aceitar uma string maliciosa.
     * 
     * O Ideal é sempre usar isso no Output e não no input.
     * 
     * @param type $value
     * @return string
     */
    protected function trataString($value)
    {
        return ValidatorUtil::stringVar($value);
    }

    /**
     * Verifica se a váriavel POST existe e a trata de forma adequada para uma string.
     * 
     * Atenção: Caso queira usar valores HTML é necessário mudar a variável opcional FILTER 
     * 
     * @param String $indice
     * @param CONST [$filter] Filtro de Sanitize que pode ser utilizado por padrão Trata uma string
     * @return misc Valor da variavel null no caso de não existir
     */
    protected function trataPost($indice, $filter = FILTER_UNSAFE_RAW)
    {
        return filter_input(INPUT_POST, $indice, $filter);
    }

    /**
     * Verifica se a váriavel GET existe e a trata de forma adequada para uma string.
     * 
     * Atenção: Caso queira usar valores HTML é necessário verificar a variável de outra maneira 
     * 
     * @param String $indice
     * @param CONST [$filter] Filtro de Sanitize que pode ser utilizado por padrão Trata uma string
     * @return misc Valor da variavel null no caso de não existir
     */
    protected function trataGet($indice, $filter = FILTER_UNSAFE_RAW)
    {
        return filter_input(INPUT_GET, $indice, $filter);
    }

    /**
     * Método que verifica se o request é um POST.
     *
     * @return boolean
     */
    public final function isPost()
    {
        return ($_SERVER['REQUEST_METHOD'] == 'POST');
    }


    /**
     * Método que retorna 400 
     * 
     * @param String $content
     */
    protected function error400($content)
    {
        $this->response($content, 400);
    }

    /**
     * 
     * @param String $content
     */
    protected function response200($content)
    {
        $this->response($content);
    }

    protected function response($content, $code = 200)
    {
        $this->setRenderizado();
        http_response_code($code);
        if (is_array($content) || is_object($content)) {
            echo json_encode($content);
        } else {
            echo $content;
        }
        exit();
    }

    /**
     * Coloca a view em modo renderizado
     *
     * @return void
     */
    protected function setRenderizado()
    {
        if (isset($this->view) && !$this->view->isRender()) {
            $this->view->setRenderizado();
        }
    }

    public function __destruct()
    {
        if (isset($this->view) && !$this->view->isRender()) {
            $this->view->render();
        }
    }

    /**
     * Exibe o logo do Enyalius para o tools e para créditos
     *
     * @return void
     */
    public function logoEny()
    {
        $this->view->setRenderizado();

        //header('Content-type: image/svg+xml');
        echo file_get_contents(CORE . 'doc/Logotipia/eny.svg');
    }

    /**
     * Informações sobre a versão do Enyalius
     *
     * @return void
     */
    public function aboutEny()
    {
        if (DEBUG) {
            $string = file_get_contents(CORE . 'enyalius');
            $inicio = strpos($string, '# Versão');
            echo 'Versão: ' . substr($string, $inicio + 10, 3);
            echo '<br> Mais informações: <a href="https://gitlab.com/enyalius">https://gitlab.com/enyalius</a>';
            $this->view->setRenderizado();
        }
    }
}

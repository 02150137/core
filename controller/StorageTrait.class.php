<?php

namespace core\controller;

/**
 *  Trait que implementa a funcionalidade de adicionar/gerenciar informações na
 *  sessão de forma transparente ao usuário tanto para tipos primitivos como para objetos
 * 
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @author Larissa Rosa
 * @version 1.0
 */
trait StorageTrait
{

    /**
     * Adiciona um dado na sessão 
     * 
     * @param String $key
     * @param misc $value
     */
    public function add($key, $value)
    {
        return \StorageUtil::add($key, $value);
    }
 
    /**
     * Método que retorna uma variável salva na sessão.
     * 
     * Retorna nulo em caso de não existencia
     * 
     * @param String $key
     * @param misc $default valor a retornar caso não encontre o dado
     * @return misc
     */
    public function get($key, $default = null)
    {
        return \StorageUtil::get($key, $default);
    }

    /**
     * Verifica se a key está setada
     *
     * @param string $key
     * @return boolean
     */
    public function verify($key)
    {
        return \StorageUtil::verify($key);
    }

    /**
     * Deleta o campo da sessão
     * 
     * @param string $key
     */
    public function del($key)
    {
        return \StorageUtil::del($key);
    }

}

Eny.Chart = function(obj){
    
    var ctx = $(obj.seletor);
    ctx.css('background-color', 'white');
    var myChart = new Chart(ctx, {
        type: obj.tipo,
        padding: 2,
        data: {
            labels: obj.xlabels,
            datasets: obj.dataset
        }
    });

    return myChart;
}
Eny.Split = function(obj) {    
    //var minSize = 20;
    var sizes = localStorage.getItem(obj.storageID); //nome da variavel para armazenar o tamanho das colunas na tela de revisao

    if (sizes) {
        sizes = JSON.parse(sizes)
    } else {
        sizes = obj.sizes // default sizes
    }
    var confs = {//id das colunas da pagina
        sizes: sizes,
        minSize: 200,
        onDragEnd: function(sizes) {
            //console.log(sizes)
            localStorage.setItem(obj.storageID, JSON.stringify(sizes))
        }
    };
    if(obj.direction){
        confs.direction = obj.direction;
    }


    return Split(obj.ids, confs);
}
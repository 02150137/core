/* 
 * Copyright (c) 2019, marcio
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
var Notifications = {
    apiAvailable: function() {
        if(window.webkitNotifications) {
            return true;
        } else {
            return false;
        }
    },
  
    isAuthorized: function() {
        if (!this.apiAvailable()) return false;
  
        return window.webkitNotifications.checkPermission() > 0 ? false : true;
    },
  
    authorize: function(callback) {
        var self = this;
        if (!this.apiAvailable()) return false;
  
        window.webkitNotifications.requestPermission(function() {
            if (self.isAuthorized()) {
                callback();
            }
        });
    },
  
    show: function(url, title, body) {
        if (!this.apiAvailable()) return false;
  
        var self = this;
  
        if (this.isAuthorized()) {
            var popup = window.webkitNotifications.createNotification(url, title, body);
            popup.show();
            setTimeout(function(){
                popup.cancel();
            }, 5000);
        } else {
            this.authorize(function() { 
                //console.log(arguments); 
                self.show(url, title, body); 
            });
        }
    },
      
    checkForPermission: function() {
        if (!this.isAuthorized()) {
            var texto = '<p>Seu navegador possui suporte a notificações. \n\
              Aperte "ALLOW" ou "PERMITIR" para a janela de notificação que irá aparecer. \n\
              <input type="button" value="Ativar notificações" /></p>';
            if(!isNull(arguments[0])){
                texto = arguments[0];
            }
            console.log("texto" + texto);
            this.callForPermission(texto);
        }
    },
      
    callForPermission: function(texto) {
          
        var authorizeBox = jQuery('<div />').addClass('notifications-authorize')
                                            .html(texto)
                                          
        jQuery('body').append(authorizeBox);
          
        jQuery('div.notifications-authorize').click(function(){
            jQuery(this).remove(); 
            Notifications.authorize();
        });
    }
};
  

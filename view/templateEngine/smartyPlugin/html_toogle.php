<?php
/**
 * Smarty plugin
 *
 * @package    Smarty
 * @subpackage PluginsFunction
 */

/**
 * Smarty {html_toogle} function plugin
 * File:       html_toogle.php<br>
 * Type:       function<br>
 * Name:       html_toogle<br>
 * Date:       26.Sep.2018<br>
 * Purpose:    Create w3c toogle component
 * <br>
 * Params:
 * <pre>
 * - name       (required) - string 
 * - value      (required) - string 
 * - label      - string 
 * </pre>
 * Examples:
 * <pre>
 * {html_toogle name="ortografia" value="true" label="Verificar ortografia"}
 * </pre>
 *
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 *
 * @param array                    $params   parameters
 * @param Smarty_Internal_Template $template template object
 *
 * @return string
 * @uses    smarty_function_escape_special_chars()
 */
function smarty_function_html_toogle($params, $template)
{
    $name = null;
    $value = null;
    $label = '';
    extract($params);
    $_checked = isset($checked) && $checked? 'checked="checked"' : ''; 

    return '<label>
        <span class="enySwitch">
     <input type="checkbox" name="'.$name.'" id="' . $name. '" value="' . $value . '" ' . $_checked . '/> 
     <span class="enySlider"></span> </span> ' . $label . '
   </label>';
}



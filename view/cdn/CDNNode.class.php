<?php
namespace core\view\cdn;

/**
 * Objeto que representa um CDN e uma lib local
 *
 * O endereço local é útil para desenvolvimento local e com conexão instável já o CDN pode ser
 * utilizado para aumentar a velocidade em sistemas online.
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0
 * @package core.view.cdn
 */
class CDNNode
{
    private $sha;
    private $endereco;
    private $nome;
    private $versao;
    private $debug = false;
    private $alternative = false;
    private $type;

    public function __construct($object = false)
    {
        if (is_array($object)) {
            $this->processaJson($object);
        } else {
            $this->endereco = $object;
        }
    }

    /**
     * Gera um CDNNode simples com dep local
     *
     * @param [type] $name
     * @param [type] $local
     * @param [type] $cdn
     * @return CDNNode
     */
    public static function makeSimpleNode($name, $local, $adress)
    {
        $cdn = new CDNNode();
        $cdn->setNome($name);
        $cdn->setAlternative($local);
        $cdn->setEndereco($adress);
        return $cdn;
    }

    /**
     * Retorna o sha caso disponível
     *
     * @return string
     */
    public function getSha()
    {
        return $this->sha;
    }

    public function getEndereco()
    {
        return $this->endereco;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function getVersao()
    {
        return $this->versao;
    }

    public function getDebug()
    {
        return $this->debug;
    }

    public function getAlternative()
    {
        return $this->alternative;
    }

    public function setSha($sha)
    {
        $this->sha = $sha;
        return $this;
    }

    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
        return $this;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    public function setVersao($versao)
    {
        $this->versao = $versao;
        return $this;
    }

    public function setDebug($debug)
    {
        $this->debug = $debug;
        return $this;
    }

    public function setAlternative($alternative)
    {
        $this->alternative = $alternative;
        return $this;
    }

    #TODO Implementar o parser de CDN ISSUE #6
    public function processaJson($obj)
    {
        if (isset($obj['cdn'])) {
            $this->setEndereco($obj['cdn']);
            if (isset($obj['sha'])) {
                $this->setSha($obj['sha']);
            }
        } else {
            throw new \InvalidArgumentException('O CDN ' . $obj . ' é inválido');
        }
    }

    public function __toString()
    {
        return $this->endereco;
    }

}

<?php
/**
 * Classe que possui o modelo de objeto para o grafico 
 */
class ChartModel implements JsonSerializable{ 
    private $idChart;
    private $xLabels = [];
    private $dataset = [];
    private $tipo = 'line';

    public function __construct($seletor = 'myChart'){
        $this->idChart = $seletor;
    }

    public function jsonSerialize():mixed
    {
        return [
            'seletor' => $this->getIdChart(),
            'tipo' => $this->getTipo(),
            'xlabels' => $this->getXLabels(),
            'dataset' => $this->getDataset()
        ];
    }

    /**
     * Get the value of idChart
     */ 
    public function getIdChart()
    {
        return $this->idChart;
    }

    /**
     * Set the value of idChart
     *
     * @return  self
     */ 
    public function setIdChart($idChart)
    {
        $this->idChart = $idChart;

        return $this;
    }

    /**
     * Get the value of tipo
     */ 
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set the value of tipo
     *
     * @return  self
     */ 
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get the value of dataset
     */ 
    public function getDataset()
    {
        return $this->dataset;
    }

    /**
     * Set the value of dataset
     *
     * @return  self
     */ 
    public function setDataset($dataset)
    {
        $this->dataset = $dataset;

        return $this;
    }

      /**
     * Set the value of dataset
     *
     * @return  self
     */ 
    public function addDataset($dataset)
    {
        $this->dataset[] = $dataset;
        return $this;
    }

    /**
     * Get the value of xLabels
     */ 
    public function getXLabels()
    {
        return $this->xLabels;
    }

    /**
     * Set the value of xLabels
     *
     * @return  self
     */ 
    public function setXLabels($xLabels)
    {
        $this->xLabels = $xLabels;

        return $this;
    }
    /**
     * Set the value of xLabels
     *
     * @return  self
     */ 
    public function addXLabel($xLabel)
    {
        $this->xLabels[] = $xLabel;

        return $this;
    }
}
<?php

/**
 * Componente que permite adicionar o CodeMirror nos projetos no enyalius.
 * 
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 */
class CodeMirror extends Componente
{
    protected static $adicionado = false;
    private static $cont = 0;
    private $theme = 'monokai';
    private $id;
    private $lang;
    private $confs;

    /**
     * Cria uma tabela a partir de um ID informado
     *
     * @param string $id
     */
    public function __construct($id)
    {
        $this->id = $id;

        if (defined('LANG')) {
            $this->lang = strtoupper(LANG);
        }
    }

    public function add()
    {
        if (!self::$adicionado) {
            $this->view->addLibJS('codemirror', '/vendor/codemirror/src/');

            $this->view->addAbsoluteCSS('/vendor/codemirror/theme/' . $this->theme . '.css');


            self::$adicionado = true;
            $this->view->addSelfScript('var codeMirror = new Array();', true);
        }
        $this->view->addSelfScript('codeMirror[' . self::$cont . '] =  CodeMirror.fromTextArea("' . $this->id . '").DataTable();
        $("' . $this->id . '").addClass("table-bordered");
        ', true);
        self::$cont++;
    }

    public function addConf($conf, $value)
    {
        $this->confs[$conf] = $value;
    }


    /**
     * Método que processa o vetor de configurações e adiciona na tabela
     *
     * @return string - objeto de configurações que podem ser adicionadas em tempo de execução
     */
    private function processaConfs()
    {
        $return = ', ';
        foreach($this->confs as $key =>$conf){
            $return .= $key . ' : ' . json_encode($conf) . ', ';
        }
        
        return rtrim($return, ', ');
    }

}

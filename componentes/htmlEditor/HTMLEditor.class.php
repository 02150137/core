<?php

/**
 * A classe HTMLEditor permite que seja adicionado o componente Froala em textAreas
 * tornando em um poderoso editor WYSIWYG HTML
 * 
 * Mais informações em https://www.summernote.org
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @package core.componentes
 * @version 1.0.0 10/12/2019
 */
class HTMLEditor extends Componente
{

    protected static $adicionado = false;
    protected static $btnAdicionado = false;

    private static $cont = 0;
    private $id;
    private $confs = [];
    private $btnSelfScript = '';
    private $botoes = [];

    public function __construct($id)
    {
        $this->id = $id;
        $this->confs = ['toolbar' => [
            ["style", []],                                        //0
            ["font", ["bold", "underline", "italic", "clear"]],   //1
            ["color", ["color"]],                                 //2
            ["para", ["ul", "ol", "paragraph"]],                  //3
            ["table", []],                                        //4
            ["insert", ["link"]],                                 //5
            ["view", ["help"]]                                    //6
        ]];
    }

    public function addBotao(SummernoteButton $bt){
        $this->confs['toolbar'][5][1][] = $bt->getName();
        $this->btnSelfScript .= $bt->getSelfScript(); 

        $this->botoes[] = $bt;       
    }

    public static function loadDeps()
    {
        Componente::registraComponente('SummernoteButton', 'htmlEditor');
        Componente::load('SummernoteButton');
    }


    /**
     * Método que adiciona o editor de vídeo.
     *
     * @return void
     */
    public function addVideo()
    {
        // ds($this->confs['toolbar']);
        if (!in_array('video', $this->confs['toolbar'][5][1])) { //previne duplicação de icone
            $this->confs['toolbar'][5][1][] = 'video';
        }
    }

    public function addCodeView()
    {
        if (!in_array('video', $this->confs[6][1])) {
            $this->confs['toolbar'][6][1] = 'codeview';
        }
    }

    /**
     * Adiciona configuração própria para o elemento
     * 
     * CUIDADO: Utilize um getConf antes para ter certeza de pegar os elementos na ordem certa,
     *  caso contrário alguns métodos podem não funcionar devido a posição dos indíces.
     *
     * @param array $obj Objeto de configuração para o editor/Summernote
     * @return void
     */
    public function setConf($obj)
    {
        $this->confs = $obj;
    }


    public function getConf()
    {
        return $this->confs;
    }

    /**
     * Adicionar o componente na página
     *
     * @return void
     */
    public function add()
    {
        if (!self::$adicionado) {
            $this->view->addLibJS('summernote-bs4.min', '/vendor/summernote/dist/');
            $this->view->addAbsoluteCSS('/vendor/summernote/dist/summernote-bs4.min.css');

            self::$adicionado = true;
            $this->view->addSelfScript('var editor = new Array();', true);        
        }
        $this->view->addSelfScript($this->btnSelfScript, true);

        $this->view->addSelfScript('editor[' . self::$cont . '] =   $(("' . $this->id . '")).summernote( ' . $this->getObjConf()  . ');', true);
        self::$cont++;
    }

    private function getObjConf(){
        $ret = json_encode($this->confs);
        if(count($this->botoes)){
            $ret = rtrim($ret, '}');
            $ret .= ', buttons:{';
            foreach($this->botoes as $bt){
                $ret .= $bt->getName() . ':' . $bt->getFunction();
            }
            $ret = rtrim($ret, ',');
            $ret .= '},
            dialogsInBody: true}'; 
        }
        return $ret;
    }
}

<?php

/**
 * Classe que permite criar um botão e adicionar ao summernote.
 * 
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 25-03-2022
 */
class SummernoteButton
{
    private $icon = 'fa fa-child';
    private $callback = '';
    private $tooltip = 'Botão';
    private $name =  'botaoTeste';
    private $iconType = 'Awesome';
    private $textIcon = '';

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function setTooltip($tooltip)
    {
        $this->tooltip = $tooltip;
    }

    /**
     * Adiciona as classes padrões do Awesome para criar o ícone
     * url: https://fontawesome.com/icons
     * 
     * ex: 'fa fa-child'
     *
     * @param [type] $icon
     * @return void
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    public function addTextIcon($text)
    {
        $this->textIcon = $text;
    }

    public function setCustomIcon($icon)
    {
        $this->icon = $icon;
        $this->iconType = 'custom';
    }

    public function getIcon()
    {
        if ($this->iconType == 'Awesome') {
            return '<i class=\"' . $this->icon . '\"/>' . $this->textIcon;
        } else {
            return  $this->icon . ' ' . $this->textIcon;
        }
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * Retorna o nome do objeto do botão criado no script.
     *
     * @return string nome da função/objeto botão
     */
    public function getFunction()
    {
        return ucfirst($this->name);
    }

    /**
     * Função que lê um arquivo JS e utiliza como corpo da função. 
     * 
     * Utiliza a constante PUBLIC_DIR para acessar o path da pasta pública e JSs 
     * do sistema. Se quiser um caminho diferente use loadBaseClickScript
     * 
     * Lembrar: O escopo desse arquivo será uma função do evento click.
     *          Existe a variável context disponível para manipulação do summernote
     *
     * @param string $file
     * @return void
     */
    public function loadClickScript($file)
    {
        $this->callback = file_get_contents(PUBLIC_DIR . 'js/' . $file);
    }

    /**
     * Função que lê um arquivo JS de qualquer lugar inclusive CDN,
     * 
     * Lembrar: O escopo desse arquivo será uma função do evento click.
     *          Existe a variável context disponível para manipulação do summernote
     *
     * @param string $file
     * @return void
     */
    public function loadBaseClickScript($file)
    {
        $this->callback = file_get_contents( $file);
    }

    public function setClickScript($script)
    {
        $this->callback = $script;
    }

    public function getSelfScript()
    {
        return 'var ' . $this->getFunction() .  ' = function(context) {
            var ui = $.summernote.ui;
        
            var button = ui.button({
                contents: "' . $this->getIcon() . '",
                tooltip: "' . $this->tooltip . '",
                click: function(e) {
                    e.preventDefault();
                    ' . $this->callback . '
                    return false;
                }
            });
        
            return button.render(); 
        }';
    }
}

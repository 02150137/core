<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/**
 * Classe com métodos que facilitam a manipulação de e-mail
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 2.0
 * @package core.util
 */
class MailUtil
{

    private static $debug = false;
    private static $name = false;
    public static $smtpSecureMode = null;

    /**
     * 
     * @param type $from
     * @param type $dest
     * @param type $title
     * @param type $message
     * @param boolean $mail [false] - Usar a função mail ao invés de SMTP
     * @return boolean
     */
    public static function send($from, $dest, $title, $message, $mail = null)
    {
        if (self::mode($mail)) {
            return self::mailFunction($from, $dest, $title, $message);
        }
        return self::smtp($from, $dest, $title, $message);
    }

    public static function addName($name)
    {
        self::$name = $name;
    }

    public static function debugOn()
    {
        self::$debug = true;
    }

    /**
     * 
     * 
     * @deprecated since version 2 use send
     * @param string $from
     * @param string $dest
     * @param string $title
     * @param string $message
     * @return boolean
     */
    public static function sendMail($from, $dest, $title, $message)
    {
        return self::smtp($from, $dest, $title, $message);
    }

    /**
     * 
     * 
     * @param string $from
     * @param string $dest
     * @param string $title
     * @param string $message
     * @return boolean
     */
    public static function smtp($from, $dest, $title, $message)
    {

        if(defined('FAKE_EMAIL') && FAKE_EMAIL){
            return true;            
        }

        $name = self::$name ? self::$name : $from; 

        $mail = new PHPMailer(TRUE);
        $mail->SetLanguage('br', CORE . 'vendor/phpmailer/phpmailer/language');
        if (self::$debug) {
            $mail->SMTPDebug = 1;                     // enables SMTP debug information (for testing)
        }
        $mail->IsSMTP();

        $mail->SMTPSecure = self::getSecure();
        $mail->CharSet = 'UTF-8';
        $mail->Host = MAIL_SERVER;
        $mail->Port = MAIL_PORT;
        $mail->SMTPAuth = true;
        $mail->Username = MAIL_USER;
        $mail->Password = MAIL_PASS;

        $mail->From = $from;
        $mail->FromName = $name;
        $eMails = is_array($dest) ? $dest : explode(',', $dest);
        foreach ($eMails as $destinatario) {
            $mail->AddAddress($destinatario);
        }
        $mail->IsHTML(true);
        $mail->Subject = $title;
        $mail->Body = nl2br($message);

        if (!$mail->Send()) {
            if (self::$debug) {
                $debug = "A mensagem não pode ser enviada" . PHP_EOL . PHP_EOL;

                $debug .= PHP_EOL . 'TITLE: ' . $title;
                $debug .= PHP_EOL . 'DESTINATARIO: ' . $dest;

                $debug .= PHP_EOL . 'MESSAGE: ' . $message;
                echo '<pre>' . $debug;
            }
            _Log::error('EMAIL NÃO ENVIADO' . $mail);
            return false;
        } else {
            return true;
        }
    }

    private static function mailFunction($from, $dest, $title, $message)
    {

        $name = self::$name ? self::$name : $from; 
        $headers = 'MIME-Version: 1.1' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        // Create email headers
        $headers .= 'From: ' . $name . "\r\n" .
                'Reply-To: ' . $from . "\r\n" .
                'X-Mailer: PHP/' . phpversion();

        $envio = mail($dest, $title, $message, $headers, "-r" . $from);

        if (self::$debug) {
            if ($envio) {
                $debug = "Mensagem enviada com sucesso" . PHP_EOL . PHP_EOL;
            } else {
                $debug = "A mensagem não pode ser enviada" . PHP_EOL . PHP_EOL;
            }

            $debug .= PHP_EOL . 'HEADERS: ' . $headers . PHP_EOL;
            $debug .= PHP_EOL . 'TITLE: ' . $title;
            $debug .= PHP_EOL . 'DESTINATARIO: ' . $dest;

            $debug .= PHP_EOL . 'MESSAGE: ' . $message;

            

            ds($debug);
        }

        return $envio;
    }

    private static function getSecure()
    {
        if(is_null(self::$smtpSecureMode)){
            if (defined('MAIL_SECURE')) {
                self::$smtpSecureMode = MAIL_SECURE;
            }
        }
        return 'tls';
    }
    
    /**
     * Método que verifica o modo de envio 
     * 
     * 1 Se for informado um valor na função send respeita ele
     * 2 Se 1 não acontecer verifica a constante MAIL_MODE
     * 3 Caso 1 e 2 não acontecer retorna true - usar função mail 
     * 
     * @param type $mail
     * @return boolean
     */
    private static function mode($mail)
    {
        if (is_null($mail)) {
            if (defined('MAIL_MODE')) {
                return MAIL_MODE == 'mail' ? true : false;
            }else{
                return true;
            }               
        }
        return $mail;
    }

}

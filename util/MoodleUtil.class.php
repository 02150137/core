<?php

/**
 * Classe estatica que facilita a manipulação do webService do moodle
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 06/07/2020
 */
class MoodleUtil
{

    /**
     * Token que permite sucessivas consultas ao web service do moodle
     * 
     * @var String
     */
    public static $token = false;
    public static $debug = false;
    public static $service = 'moodle_mobile_app';

    /**
     *
     * @var String 
     */
    public static $site = 'https://moodle.canoas.ifrs.edu.br';

    public static function geraToken($usuario, $senha)
    {
        $url = self::$site . '/login/token.php';
       // $url .= '?service=moodle_mobile_app&username=' . $usuario . '&password=' . $senha ;

        $postdata = http_build_query(
            array(
                'username' => $usuario,
                'password' => $senha,
                'service' =>self::$service
            )
        );
        
        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-Type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );
        
        $context  = stream_context_create($opts);

        $result = file_get_contents($url, false, $context);
        //ds($url, $result);

        return json_decode($result);
    }

    /**
     * 
     * 
     * @param String $servico
     * @param Array(optional) $extras
     * @param String(optional) $token 
     * @return stdObject Objeto de retorno do serviço
     */
    public static function requisita($servico, $extras = '', $token = false)
    {
        $server = isset($_SESSION['moodleServer']) ? $_SESSION['moodleServer'] : self::$site;    
        $tokenOK = $token ? $token : self::$token;
        $url = $server . '/webservice/rest/server.php?wsfunction=' . $servico . self::preparaARGS($extras) . '&wstoken=' . $tokenOK . '&moodlewsrestformat=json';
        if(self::$debug){
            $url2 = $server . '/webservice/rest/server.php?wsfunction=' . $servico . self::debugARGS($extras) . '&wstoken=' . $tokenOK . '&moodlewsrestformat=json';
            ds($url2);
        }
        _LOG::put($url, LOGS . '/moodlelog.log');
        return json_decode(file_get_contents($url));
    }
    
    private static function preparaARGS($extras){
        if(is_string($extras)){
            $extrasBuild = $extras; 
        }else{
            setlocale(LC_ALL, 'us_En');
            $extrasBuild = http_build_query($extras);
        }
        return !empty($extrasBuild) ? '&' . $extrasBuild : '';
    }
    
     private static function debugARGS($extras){
        if(is_string($extras)){
            $extrasBuild = $extras; 
        }else{
            setlocale(LC_ALL, 'us_En');
            $extrasBuild = urldecode(http_build_query($extras));
        }
        return !empty($extrasBuild) ? '&' . $extrasBuild : '';
    }


}

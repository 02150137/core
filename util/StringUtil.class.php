<?php

/**
 * Classe Util que permite manipular strings entre os padrões underscore e camelCase
 * a mesma possui funcionalidades uteis como remoção de acentos 
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 * @package core.util
 */
abstract class StringUtil
{

    protected static $charMap = array(
        'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ç' => 'C',
        'É' => 'E', 'Ê' => 'E', 'Ì' => 'I', 'Í' => 'I',
        'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ő' => 'O',
        'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U',
        'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ç' => 'c',
        'é' => 'e', 'ê' => 'e',
        'í' => 'i', 'ñ' => 'n',
        'ó' => 'o', 'ô' => 'o', 'õ' => 'o',
        'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u'
    );

    /**
     * Convert strings with underscores into CamelCase
     *
     * @param    string    $input    The string to convert
     * @return    string    The converted string
     */
    public static function toUnderscore($input)
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }

    /**
     * Método que remove especial char
     *
     * @param [type] $input
     * @return string
     */
    public static function removeEspecialChar($input)
    {
        return preg_replace('/[\W]/', '', $input);
    }

    public static function underscoreNumber($input)
    {
        $text = self::toUnderscore($input);
        $text = preg_replace('/[0-9]+/', '_$0_', $text);
        $text = str_replace('__', '_', $text);
        return trim($text, '_');
    }

    /**
     * Método que criar uma url amigavel conhecida como slugfy
     * 
     * Ex: Métodos de escrita
     *     metodos-de-escrita
     *
     * @param [type] $text
     * @param string $divider
     * @return string
     */
    public static function slugify($text, string $divider = '-')
    {
        $text = self::removeAcentuacao($text);

        #https://stackoverflow.com/questions/2955251/php-function-to-make-slug-url-string
        // replace non letter or digits by divider
        $text = preg_replace('~[^\pL\d]+~u', $divider, $text);

        // transliterate Ver o que esse acara faz
        #https://github.com/nunomaduro/phpinsights/issues/43
        #$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text); 

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, $divider);

        // remove duplicate divider
        $text = preg_replace('~-+~', $divider, $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    /**
     * Convert strings with underscores into CamelCase
     *
     * @param    string    $input    The string to convert
     * @param    bool    $first_char_caps    camelCase or CamelCase
     * @return    string    The converted string
     */
    public static function toCamelCase($input, $first_char_caps = false)
    {
        if ($first_char_caps == true) {
            $input[0] = strtoupper($input[0]);
        }
        return preg_replace_callback(
            '/_([a-z0-9])/',
            function ($c) {
                return strtoupper($c[1]);
            },
            $input
        );
    }

    /**
     * Convert strings with spaces into CamelCase
     *
     * @param string $input    The string to convert
     * @param bool $first_char_caps    camelCase or CamelCase
     * @return string The converted string
     */
    public static function spaceToCamelCase($input, $first_char_caps = false)
    {
        if ($first_char_caps == true) {
            $input[0] = strtoupper($input[0]);
        }
        $string= preg_replace_callback(
            '/ ([a-z0-9])/',
            function ($c) {
                return strtoupper($c[1]);
            },
            $input
        );
        return str_replace(' ', '', $string);
    }

    /**
     * Função que substitui um vetor de strings em um texto
     *
     * @param [type] $string
     * @param [type] $dicionario
     * @return void
     */
    public static function replaceByDictionary($string, $dicionario)
    {
        $aux = str_replace('_', ' ', $string);
        foreach ($dicionario as $str => $strSub) {
            $aux = str_replace($str, $strSub, $aux);
        }
        return $aux;
    }

    /**
     * Função que remove acentuação das palavras exemplo.
     * função => funcao
     * 
     * @param String $string
     * @return String
     */
    public static function removeAcentuacao($string)
    {
        return strtr($string, self::$charMap);
    }

    public static function extractLenght($stringCompleta, $textoSearch, $end = 1)
    {
        $pos = stripos($stringCompleta, $textoSearch);
        $str = substr($stringCompleta, $pos, strlen($textoSearch) + $end);
        $strRemove = str_replace($textoSearch, '', $str);
        $unit = trim($strRemove); // remove whitespaces
        return $unit;
    }

    /**
     * Implementar um simples stemmer
     *
     * @param [type] $string
     * @param string $lang
     * @return void
     */
    public static function singular($string, $lang = 'pt-br')
    {
    }
}

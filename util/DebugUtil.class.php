<?php

/**
 * Description of DebugUtil
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 * @package 
 */
class DebugUtil
{

    public static function show($misc)
    {
        echo '<pre style="position:relative; z-index:999999; border:1px dashed red; background-color: #cfc;">';
        $pilha = debug_backtrace();
        $type = !is_string($misc) && is_callable($misc) ? "Callable" : ucfirst(gettype($misc));
        $varName =  self::varName($pilha[1]['line'], $pilha[1]['file']);
        echo '*' . $pilha[1]['file'] . ': <span style="color:red">' . $pilha[1]['line'] . '</span>: ' . $varName. '(' . $type . ") = <strong>";
        
        if (is_array($misc) || is_object($misc)) {
            echo PHP_EOL;

            print_r($misc);
        } else {
            $v = var_export($misc);
            echo $v;
        }
        echo '</strong></pre>';
    }

    private static function varName($line, $file)
    {
        $vLine = file($file);
        $fLine = $vLine[$line - 1];
        preg_match("#\\$(\w+)#", $fLine, $match);
        //ver de usar o yeld com iteratora
        return isset($match[0]) ? $match[0] : 'LITERAL';
    }

    public static function showCode($misc)
    {
        echo '<pre><code>';
        echo $misc;
        echo '</code></pre>';
    }

    public static function remoteShow($misc)
    {
        ob_start();
        self::show($misc);
        $result = ob_get_clean();
       // MailUtil::debugMail($from, $dest, $title, $message);
    }

}
